FROM python:3.6
LABEL maintainer="alexey.chasovnikov@gmail.com"
EXPOSE 5000
COPY . .
WORKDIR ./python-sample-app
RUN pip install -r requirements.txt
ENV FLASK_APP=app.py \
    POSTGRESQL_URL=postgresql://worker:password@postgres/db
ENTRYPOINT ["python3", "app.py"]
